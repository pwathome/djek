#lots of tips provided by these resources:
#https://nodejs.org/en/docs/guides/nodejs-docker-webapp/
#https://github.com/nodejs/docker-node/blob/master/docs/BestPractices.md
#https://blog.hasura.io/an-exhaustive-guide-to-writing-dockerfiles-for-node-js-web-apps-bbee6bd2f3c4
#https://github.com/stephenafamo/adonisjs-docker
#https://medium.com/@kopahead/docker-loves-adonis-baa8569662f1

#install node image
FROM node:16-alpine

#place global npm dependencies in the non-root user directory
ENV NPM_CONFIG_PREFIX=/home/node/.npm-global
ENV PATH=$PATH:/home/node/.npm-global/bin

# #also set shared env variables that will be the same across instances
# ENV CACHE_VIEWS=false
# ENV DB_CONNECTION=mysql
# ENV DB_PORT=3306
ENV HOST=0.0.0.0
# ENV LTI_KEY=boost
ENV PORT=80
# ENV SESSION_DRIVER=cookie

# To handle 'not get uid/gid'
# See: https://stackoverflow.com/questions/52196518/could-not-get-uid-gid-when-building-node-docker
RUN npm config set unsafe-perm true

#Install nodemon for hot reload, adonis CLI (database migrations, etc.), and pm2 for server
RUN npm install -g nodemon @adonisjs/cli pm2

#recommended workdir for node images
WORKDIR /home/node/app

#add the (optional) .env to the directory to access variables
# ADD *.env ./

#expose port defined in .env file
EXPOSE 80

#install dependencies
COPY package*.json ./
RUN npm install

#copy rest of app into work dir
COPY . ./

#npm script to build webpack resources
RUN npm run build

RUN npm install -g serve

#ensure we are not running as root user for security; node image provides this user instead
USER node

#start app
CMD ["serve", "-s", "build"]
